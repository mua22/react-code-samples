import { ADD_TODO } from "./../constants/action-types";

export function addTofo(payload) {
  return { type: ADD_TODO, payload };
}
